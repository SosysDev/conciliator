module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('rules', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      array_fields: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
        allowNull: false,
      },
      method_id: {
        type: Sequelize.INTEGER,
        references: { model: 'methods', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('rules');
  },
};
