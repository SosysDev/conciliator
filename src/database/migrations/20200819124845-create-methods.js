module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('methods', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      first_array: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      second_array: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      module_id: {
        type: Sequelize.INTEGER,
        references: { model: 'modules', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('methods');
  },
};
