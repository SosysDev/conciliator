import Sequelize from 'sequelize';

import User from '../app/models/User';
import Client from '../app/models/Client';
import Group from '../app/models/Group';
import Module from '../app/models/Module';
import Method from '../app/models/Method';
import Rule from '../app/models/Rule';

import databaseConfig from '../config/database';

const models = [User, Client, Group, Module, Method, Rule];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
