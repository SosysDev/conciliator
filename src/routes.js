import { Router } from 'express';

import SessionController from './app/controllers/SessionController';
import UserController from './app/controllers/UserController';
import ClientController from './app/controllers/ClientController';
import GroupController from './app/controllers/GroupController';
import ModuleController from './app/controllers/ModuleController';
import MethodController from './app/controllers/MethodController';
import RuleController from './app/controllers/RuleController';

import authMiddleware from './app/middleware/auth';

const routes = new Router();

routes.post('/users', UserController.store);
routes.post('/sessions', SessionController.store);

routes.use(authMiddleware);

routes.put('/users', UserController.update);

routes.post('/clients', ClientController.store);
routes.get('/clients', ClientController.list);

routes.post('/groups', GroupController.store);
routes.get('/groups', GroupController.list);

routes.post('/modules', ModuleController.store);
routes.get('/modules', GroupController.list);

routes.post('/methods', MethodController.store);
routes.get('/methods', GroupController.list);

routes.post('/rules', RuleController.store);
routes.put('/rules/:id', RuleController.update);
routes.get('/rules', GroupController.list);

export default routes;
