import Sequelize, { Model } from 'sequelize';

class Rule extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        array_fields: Sequelize.ARRAY(Sequelize.TEXT),
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Method, { foreignKey: 'method_id', as: 'method' });
  }
}

export default Rule;
