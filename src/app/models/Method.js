import Sequelize, { Model } from 'sequelize';

class Method extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        first_array: Sequelize.STRING,
        second_array: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Module, { foreignKey: 'module_id', as: 'module' });
  }
}

export default Method;
