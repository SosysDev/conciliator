import * as Yup from 'yup';
import Method from '../models/Method';

class MethodController {
  async list(req, res) {
    const methods = await Method.findAll();

    return res.json(methods);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      module_id: Yup.number().required(),
      first_array: Yup.string().required(),
      second_array: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const methodExists = await Method.findOne({
      where: { name: req.body.name, module_id: req.body.module_id },
    });

    if (methodExists) {
      return res.status(400).json({ error: 'Method already exists' });
    }

    const { id, name, first_array, second_array } = await Method.create(
      req.body
    );

    return res.json({ id, name, first_array, second_array });
  }

  async update(req, res) {
    return res.json();
  }
}

export default new MethodController();
