import * as Yup from 'yup';
import Module from '../models/Module';

class ModuleController {
  async list(req, res) {
    const modules = await Module.findAll();

    return res.json(modules);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      group_id: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const moduleExists = await Module.findOne({
      where: { name: req.body.name, group_id: req.body.group_id },
    });

    if (moduleExists) {
      return res.status(400).json({ error: 'Module already exists' });
    }

    const { id, name } = await Module.create(req.body);

    return res.json({ id, name });
  }

  async update(req, res) {
    return res.json();
  }
}

export default new ModuleController();
