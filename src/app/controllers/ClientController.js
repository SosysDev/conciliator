import * as Yup from 'yup';
import Client from '../models/Client';

class ClientController {
  async list(req, res) {
    const clients = await Client.findAll();

    return res.json(clients);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const clientExists = await Client.findOne({
      where: { name: req.body.name },
    });

    if (clientExists) {
      return res.status(400).json({ error: 'Client already exists' });
    }

    const { id, name } = await Client.create(req.body);

    return res.json({ id, name });
  }

  async update(req, res) {
    return res.json();
  }
}

export default new ClientController();
