import * as Yup from 'yup';
import Rule from '../models/Rule';

class RuleController {
  async list(req, res) {
    const rules = await Rule.findAll({
      include: ['id', 'name', 'created_at', 'updated_at'],
    });

    return res.json(rules);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      method_id: Yup.number().required(),
      array_fields: Yup.array().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const ruleExists = await Rule.findOne({
      where: { name: req.body.name, method_id: req.body.method_id },
    });

    if (ruleExists) {
      return res.status(400).json({ error: 'Rule already exists' });
    }

    const { id, name, array_fields } = await Rule.create(req.body);

    return res.json({ id, name, array_fields });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      array_fields: Yup.array().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const rule = await Rule.findByPk(req.params.id);

    if (!rule) {
      return res.status(400).json({ error: `Rule don't exists` });
    }

    await rule.update(req.body);

    return res.json('Rule updated');
  }
}

export default new RuleController();
