import * as Yup from 'yup';
import Group from '../models/Group';

class GroupController {
  async list(req, res) {
    const groups = await Group.findAll();

    return res.json(groups);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      client_id: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const groupExists = await Group.findOne({
      where: { name: req.body.name, client_id: req.body.client_id },
    });

    if (groupExists) {
      return res.status(400).json({ error: 'Group already exists' });
    }

    const { id, name } = await Group.create(req.body);

    return res.json({ id, name });
  }

  async update(req, res) {
    return res.json();
  }
}

export default new GroupController();
