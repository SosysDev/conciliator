import * as Yup from 'yup';
import Rule from '../models/Rule';

class ValidationController {
  async store(req, res) {
    return res.json();
  }

  async update(req, res) {
    return res.json();
  }
}

export default new ValidationController();
